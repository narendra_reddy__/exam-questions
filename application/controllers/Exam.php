<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
	{
		parent::__construct();
		 $this->load->model('exam_model');
	} 
	public function index()
	{
		$data = $this->exam_model->getQue(rand(1,10),1); 
		//echo "<pre>" ; print_r($data['qns']['qn']) ;die;
		$this->load->view('exam/index',$data);
	}
	public function append()
	{
	    //$res['queId'] 	= 	$_REQUEST['queId'];
	    $res = $this->exam_model->getQue($_REQUEST['queId']); 
		$response["body"]		=	$this->load->view('exam/qu',$res,TRUE);
		echo json_encode($response);
	}
	public function nameSave()
	{
		$data	=	$this->exam_model->nameSave($_REQUEST['name']);
		echo json_encode($response);
	}
	public function selectedSave()
	{
		$data	=	$this->exam_model->selectedSave($_REQUEST['val'],$_REQUEST['id']);
	}
	public function result()
	{
		$res['correct'] = $this->exam_model->result(1) ;
		$res['name'] = $this->exam_model->getSingleField() ;
		$res['wrong'] = $this->exam_model->result(0) ;
		$res['skiped'] = (10-($res['correct']+$res['wrong'])) ; 
		$response["body"]		=	$this->load->view('exam/result',$res,TRUE);
		echo json_encode($response);
	}
}
