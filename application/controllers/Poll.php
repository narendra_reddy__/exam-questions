<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poll extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
	{
		parent::__construct();
		 $this->load->model('poll_model');
	} 
	public function index()
	{
		$data = $this->poll_model->getData();
		//echo "<pre>" ;print_r($res) ;die;
		$this->load->view('poll/index',$data);
	}
	public function save()
	{
		$data=$this->poll_model->savePoll();
		if($data==403){
			$this->session->set_flashdata("user_error","Please select at least one...");
			redirect('poll');
		}
		if($data == 200){
			$this->session->set_flashdata("user_success","You voted successfully...");
			redirect('poll');
		}else{
			$this->session->set_flashdata("user_error","Failed please try again...");
			redirect('poll');
		}
	}
}
