-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2021 at 12:52 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `study`
--

-- --------------------------------------------------------

--
-- Table structure for table `ans`
--

CREATE TABLE `ans` (
  `id` int(11) NOT NULL,
  `qn_id` int(11) NOT NULL,
  `ans` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ans`
--

INSERT INTO `ans` (`id`, `qn_id`, `ans`) VALUES
(1, 1, 3),
(2, 2, 1),
(3, 3, 4),
(4, 4, 1),
(5, 5, 2),
(6, 6, 3),
(7, 7, 4),
(8, 8, 2),
(9, 9, 1),
(10, 10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `options` varchar(150) NOT NULL,
  `qn_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `options`, `qn_id`) VALUES
(1, 'list', 1),
(2, 'ol', 1),
(3, 'ul', 1),
(4, 'dl', 1),
(5, 'list 12', 2),
(6, 'ol 12', 2),
(7, 'ul 12', 2),
(8, 'dl 12', 2),
(9, 'list321', 3),
(10, 'mange', 3),
(11, 'ul32', 3),
(12, 'dl32', 3),
(13, 'tata', 4),
(14, 'bhar', 4),
(15, 'busi', 4),
(16, 'bazar', 4),
(17, 'movie', 5),
(18, 'cinema', 5),
(19, 'heroin', 5),
(20, 'comedy', 5),
(21, 'aple', 6),
(22, 'mango', 6),
(23, 'bana', 6),
(24, 'fine', 6),
(25, 'ball', 7),
(26, 'bat', 7),
(27, 'wick', 7),
(28, 'out', 7),
(29, 'bed', 8),
(30, 'sleep', 8),
(31, 'night', 8),
(32, 'ten', 8),
(33, 'zro', 9),
(34, 'one', 9),
(35, 'two', 9),
(36, 'three', 9),
(37, 'four', 10),
(38, 'five', 10),
(39, 'six', 10),
(40, 'seve', 10),
(41, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `que`
--

CREATE TABLE `que` (
  `id` int(11) NOT NULL,
  `qn` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `que`
--

INSERT INTO `que` (`id`, `qn`) VALUES
(1, 'Who is making the Web standards?'),
(2, 'Choose the correct HTML element for the largest heading:'),
(3, 'What is the correct HTML element for inserting a line break?'),
(4, 'What is the correct HTML for adding a background color?'),
(5, 'Choose the correct HTML element to define important text'),
(6, 'Choose the correct HTML element to define emphasized text'),
(7, 'What is the correct HTML for creating a hyperlink?'),
(8, 'Which character is used to indicate an end tag?'),
(9, 'How can you open a link in a new tab/browser window?'),
(10, 'Which of these elements are all <table> elements?');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `result` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `qn_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `result`, `user_id`, `qn_id`) VALUES
(1, 0, 5, 1),
(2, 0, 5, 2),
(3, 0, 5, 3),
(4, 0, 5, 4),
(5, 1, 5, 5),
(6, 1, 5, 6),
(7, 0, 5, 7),
(8, 0, 5, 8),
(9, 0, 5, 9),
(10, 0, 5, 10),
(11, 0, 6, 1),
(12, 0, 6, 3),
(13, 1, 6, 4),
(14, 0, 6, 5),
(15, 0, 6, 6),
(16, 0, 6, 7),
(17, 0, 6, 8),
(18, 0, 6, 9),
(19, 0, 6, 10),
(20, 0, 7, 1),
(21, 0, 7, 3),
(22, 0, 7, 4),
(23, 0, 7, 6),
(24, 0, 7, 7),
(25, 0, 7, 8),
(26, 0, 7, 9),
(27, 1, 7, 10),
(28, 0, 8, 1),
(29, 1, 8, 10),
(30, 0, 9, 1),
(31, 1, 9, 10),
(32, 0, 10, 1),
(33, 1, 10, 2),
(34, 0, 10, 3),
(35, 0, 10, 4),
(36, 1, 10, 6),
(37, 0, 10, 9),
(38, 1, 10, 10),
(39, 1, 11, 1),
(40, 0, 12, 1),
(41, 0, 12, 2),
(42, 0, 12, 3),
(43, 0, 12, 4),
(44, 0, 12, 5),
(45, 1, 12, 6),
(46, 0, 12, 7),
(47, 0, 12, 8),
(48, 0, 12, 9),
(49, 0, 12, 10),
(50, 0, 13, 1),
(51, 0, 13, 3),
(52, 1, 13, 10),
(53, 0, 14, 2),
(54, 0, 14, 3),
(55, 0, 14, 4),
(56, 0, 14, 5),
(57, 0, 14, 6),
(58, 1, 14, 7),
(59, 0, 17, 4),
(60, 1, 17, 5),
(61, 0, 17, 6),
(62, 1, 21, 1),
(63, 1, 22, 8),
(64, 1, 23, 8),
(65, 0, 23, 2),
(66, 0, 24, 3),
(67, 0, 24, 2),
(68, 0, 24, 4),
(69, 0, 24, 5),
(70, 1, 24, 6),
(71, 0, 24, 7),
(72, 1, 24, 8),
(73, 0, 24, 9),
(74, 0, 24, 10),
(76, 0, 29, 3),
(77, 0, 29, 4),
(78, 0, 29, 5),
(79, 0, 29, 6),
(80, 0, 29, 7),
(81, 0, 29, 8),
(82, 0, 29, 9),
(83, 0, 29, 10),
(84, 0, 31, 8);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`) VALUES
(1, 'Test123'),
(2, 'Test123'),
(3, 'tyest123'),
(4, 'Test123'),
(5, 'Test123'),
(6, 'narendra'),
(7, 'narendra'),
(8, 'tyest123'),
(9, 'narendra'),
(10, 'ramya'),
(11, 'narendra'),
(12, 'narendra'),
(13, 'test123'),
(14, 'Dummy'),
(15, 'narendra'),
(16, 'hhhh'),
(17, 'ygyitituiut'),
(18, 'tyest123'),
(19, 'tyest123'),
(20, 'tyest123'),
(21, 'test123'),
(22, 'ramya'),
(23, 'tyest123'),
(24, 'narendra'),
(25, 'tyest123'),
(26, 'tyest123'),
(27, 'tyest123'),
(28, 'tyest123'),
(29, 'tyest123'),
(30, 'My Name'),
(31, 'test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ans`
--
ALTER TABLE `ans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `que`
--
ALTER TABLE `que`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ans`
--
ALTER TABLE `ans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `que`
--
ALTER TABLE `que`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
